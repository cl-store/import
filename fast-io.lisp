(in-package :cl-store)
(declaim (optimize speed (safety 0) (space 0) (debug 0) (compilation-speed 0)))
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant +buffer-size+ 4096))

(deftype buf () `(simple-array (unsigned-byte 8) (,+buffer-size+)))
(deftype buf-size () `(integer 0 ,+buffer-size+))

(defvar *full-write* t
  "An evil, evil variable. Read sequence doesn't just block it also
waits until the buffer has been filled. This forces the full
4096 bytes stored in the buffer to be written. Set this
to nil if you don't like file sizes being multiples of 4096
when writing to files. This should be removed, or at least
deprecated, when a better solution is found.")


;; A structure was chosen over a normal object
;; to get speed improvements on sbcl (and cmucl i guess)
(defstruct (buffer (:conc-name buf-)
                   (:print-function
                    (lambda (obj stream depth)
                      (declare (ignore depth))
                      (print-unreadable-object (obj stream :type t :identity t)))))
  (stream nil)
  (ibuf (make-array +buffer-size+ :element-type '(unsigned-byte 8)
                    :initial-element 0 :adjustable nil)
        :type buf)
  (ibytes 0 :type buf-size)
  (ioffset 0 :type buf-size)
  (obuf (make-array +buffer-size+ :element-type '(unsigned-byte 8)
                    :initial-element 0 :adjustable nil)
        :type buf)
  (obytes 0 :type buf-size)
  (ooffset 0 :type buf-size))


;; reading
;; how should EOF be handled??


;;(declaim (ftype (function (buffer) (unsigned-byte 8)) read-buf-byte))
(defgeneric read-buf-byte (buf))

(defmethod read-buf-byte ((buf buffer))
  (declare (type buffer buf))
  (unless (< (buf-ioffset buf) (buf-ibytes buf))
    (fill-buffer buf))
  (prog1
      (aref (buf-ibuf buf) (buf-ioffset buf))
    (incf (buf-ioffset buf))))
  
(defgeneric fill-buffer (buf))

(defmethod fill-buffer ((buf buffer))
  (setf (buf-ibytes buf)
        (read-sequence (buf-ibuf buf) (buf-stream buf)))
  (setf (buf-ioffset buf) 0)
  (values))


;; writing
(defgeneric write-buf-byte (byte buf))

(defmethod write-buf-byte (byte (buf buffer))
  (declare (type buffer buf) (type (unsigned-byte 8) byte))
  (unless (< (buf-ooffset buf) +buffer-size+)
    (flush buf))
  (prog1
      (setf (aref (buf-obuf buf) (buf-ooffset buf)) byte)
    (incf (buf-ooffset buf))))

(defgeneric flush (buf))

(defmethod flush ((buf buffer))
  (write-sequence (buf-obuf buf) (buf-stream buf)
                  :start 0 :end (if *full-write*
                                    +buffer-size+
                                    (buf-ooffset buf)))
  (setf (buf-ooffset buf) 0)
  (force-output (buf-stream buf)))


;; EOF
