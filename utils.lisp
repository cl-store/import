;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;; See the file LICENCE for licence information.

(in-package :cl-store)
(defmacro aif (test conseq &optional (else nil))
  `(let ((it ,test))
     (declare (ignorable it))
     (if it ,conseq
         (macrolet ((setf-it (val) (list 'setf ',test val)))
           ,else))))

(defmacro with-gensyms (names &body body)
  `(let ,(mapcar #'(lambda (x) `(,x (gensym))) names)
     ,@body))

(defvar *store-class-slots* t
  "Whether or not to serialize class allocation slots.")

(defun object-slot-and-vals (object)
  "Create a plist containing slot names and values
for all bound slots in OBJECT. If *store-class-slots* is not
null then include slots which are class allocated."
  (remove-if
   #'null
   (mapcar #'(lambda (x)
               (let ((slot-name (slot-definition-name x)))
                 (when (and (slot-boundp object slot-name)
                            (or *store-class-slots*
                                (and (not *store-class-slots*)
                                     (eq (slot-definition-allocation x)
                                         :class))))
                   (list slot-name
                         (slot-value object slot-name)))))
           (compute-slots (class-of object)))))


(defun group (source n)
  "Group from Paul Graham's on Lisp."
  (declare (fixnum n))
  (if (zerop n) (error "N is zero, must be a positive fixnum."))
  (labels ((rec (source acc)
             (let ((rest (nthcdr n source)))
               (if (consp rest)
                   (rec rest (cons (subseq source 0 n) acc))
                   (nreverse (cons source acc))))))
      (rec source nil)))

(defun group-array (values subscripts)
  "Group VALUES, a flattened list of array values, into a suitable
list to be used as :initial-contents to make-array according to SUBSCRIPTS."
  (if (cdr subscripts)
      (group-array (group values (car subscripts)) (cdr subscripts))
      values))

(defun get-array-values (array)
  "Returns a suitable list to be used for :initial-contents
or :initial-element to make-array"
  (when (zerop (array-total-size array))
    (return-from get-array-values nil))
  (let ((val (loop for x from 0 to (1- (array-total-size array))
               collect (row-major-aref array x))))
    (declare (type list val))
    (if (every #'(lambda (x) (equal x (car val))) val)
        `(:initial-element ,(car val))
        `(:initial-contents ,(group-array
                              val
                              (nreverse (array-dimensions array)))))))


(defun get-slot-details (slot-definition)
  (list :name (slot-definition-name slot-definition)
        :allocation (slot-definition-allocation slot-definition)
        :initargs (slot-definition-initargs slot-definition)
        ;; :initform. dont use initform until we can
        ;; serialize functions
        :readers (slot-definition-readers slot-definition)
        :type (slot-definition-type slot-definition)
        :writers (slot-definition-writers slot-definition)))

(defun get-class-details (x)
  (list (class-name x)
        (class-direct-default-initargs x)
        (mapcar #'get-slot-details (class-direct-slots x))
        (mapcar #'class-name
                (class-direct-superclasses x))
        (type-of x)))


;; where this package seems to spend a large portion of its time
(defun circular-listp (x)
  (handler-case (not (list-length x))
    (type-error (c) (declare (ignore c)) nil)))


(defmacro awhen (test &body body)
  `(aif ,test
        (progn ,@body)))


;; because clisp doesn't have the class single-float or double-float.
(defun float-type (float)
  (typecase float
    (single-float 0)
    (double-float 1)
    (t 0)))

(defun get-float-type (num)
  (case num
    (0 1.0)
    (1 1.0d0)))


;; EOF
