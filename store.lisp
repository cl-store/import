;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;; See the file LICENCE for licence information.
#|
              CL-STORE : A Common Lisp Serialization Package.


See the file LICENCE for licence information.

TODO:
=====
- Add some sort of EOF mechanism.

- fix up circularity stuff so that eq floats are restored correctly.

- hopefully find a better way to do circularity fixing

- structure storing for non python implementations
- structure definitions 

- Store functions.
- Store closures.

- documentation!



NOTES:
=======
- Should package storing store the actual
  package details (symbols, use list etc)
  or remain as find-package.

- turned off circularity fixing of floats
  since the rehash-size of hash-tables in cmucl are
  eq and this crashes restoration of nested hash-tables.

- Restoring class objects does not quite work right
  since we cannot serialize functions and this
  destroys serializing the :initfunction for
  a class. This basically means that after
  making an instance of the restored class
  all slots will be unbound, unless they are class
  allocated .

- Objects with required arguments will fail on restoration.
  (Although that will be done in the initfunction and
   due to the above problem this will only be an issue
   when functions can be serialized.)
   This could probably be worked around with allocate-instance
   and friends.

- Pathname device and host are not stored with pathnames.
  What is a reasonable approach for this?

- circularity fixing could be cleaned up somewhat. 


entry-points
------------
Generic Function:  store (obj stream|file) => obj
Generic Function:  restore (stream|file) => obj   ;; setf-able

|#

(in-package :cl-store)

(defvar +store-magic-number+ 1912923)
(defvar *registered-types* (make-hash-table))
(defvar *registered-type-counter* 0)
(defvar *restore-funs* (make-hash-table))
(defvar *nuke-existing-classes* nil
  "Do we overwrite existing class definitions on restoration.")
(defvar *store-class-superclasses* t
  "Whether or not to store the superclasses of a stored class")

(defvar *need-to-fix* nil)
(defvar *to-eval* nil)


;; entry points


(defun restore-file (place)
  (with-open-file (s place :direction :input :element-type '(unsigned-byte 8)
                     :if-does-not-exist nil)
    (when s
      (restore (make-buffer :stream s)))))

(defgeneric restore (place)
  (:method ((place string))
           "Restore the object found in the String PLACE."
           (restore-file place))
  (:method ((place pathname))
           "Restore the object found in Pathname PLACE."
           (restore-file place))
  (:method ((place stream))
           "Restore the object found in STREAM STREAM"
           (restore (make-buffer :stream place)))
  (:method ((place buffer))
           "Restore the object found in Stream PLACE."
           (check-magic-number place)
           (let* ((*stored-counter* 0)
                  (*need-to-fix* nil)
                  (*stored-values* (make-hash-table))
                  (*to-eval* nil)
                  (obj (restore-object place)))
             (when *need-to-fix*
               (fix-circularities *stored-values* obj))
             (dolist (x *to-eval*) (eval x))
             obj)))

(defun (setf restore) (new-val place)
  (store new-val place))

(defun store-file (obj place)
  (with-open-file (stream place :direction :output :if-exists :supersede
                          :element-type '(unsigned-byte 8))
    (store obj (make-buffer :stream stream))))


(defgeneric store (obj place)
  (:method (obj (place pathname))
           "Store OBJ into Pathname PLACE."
           (store-file obj place))
  (:method (obj (place string))
           "Store OBJ into String PLACE."
           (store-file obj place))
  (:method (obj (place stream))
           "Store OBJ into STREAM"
           (store obj (make-buffer :stream place)))
  (:method (obj (place buffer))
           "Store OBJ into Stream PLACE."
           (let ((*stored-counter* 0)
                 (*stored-values* (make-hash-table :test #'eq)))
             (store-32byte +store-magic-number+ place)
             (store-object obj place)
             (flush place)
             obj)))

(defgeneric internal-store-object (obj buff)
  (:method ((obj t) (place buffer))
           "If call falls back here then OBJ cannot be serialized"
           (error 'store-error
                  :datum "Cannot store objects of type ~D."
                  :args (type-of obj))))




;; conditions
(define-condition store-error ()
  ((datum :accessor datum :initarg :datum :initform "Unknown")
   (args :accessor args :initarg :args :initform nil))
  (:report (lambda (condition stream)
             (apply #'format stream (datum condition) (args condition) nil))))

(define-condition restore-error ()
  ((datum :accessor datum :initarg :datum :initform "Unknown")
   (args :accessor args :initarg :args :initform nil))
  (:report (lambda (condition stream)
             (apply #'format stream (datum condition) (args condition) nil))))



(defun check-magic-number (stream)
  "Check to see if STREAM actually contains a stored object."
  (let ((val (read-32-byte stream)))
    (unless (= +store-magic-number+ val)
      (error 'restore-error
             :datum "Stream does not contain a stored object."))))

(defun read-32-byte (buf &optional (signed t))
  "Read a signed or unsigned byte off STREAM."
  (let ((byte1 (read-buf-byte buf))
        (byte2 (read-buf-byte buf))
        (byte3 (read-buf-byte buf))
        (byte4 (read-buf-byte buf)))
    (let ((ret (+ byte1 (* 256 (+ byte2 (* 256 (+ byte3 (* 256 byte4))))))))
      (if (and signed (> byte1 127))
          (logior (ash -1 32) ret)
          ret))))

(defun store-32byte (obj buf)
  "Write OBJ down STREAM as a 32 byte integer."
  (write-buf-byte (ldb (byte 8 0) obj) buf)
  (write-buf-byte (ldb (byte 8 8) obj) buf)
  (write-buf-byte (ldb (byte 8 16) obj) buf)
  (write-buf-byte (+ 0 (ldb (byte 8 24) obj)) buf))


(defun register-code (type)
  (aif (gethash type *registered-types*)
       it
       (setf-it (incf *registered-type-counter*))))

(defmacro defstore ((var type buffer &rest method-args) &body body)
  "Defines method store-object specialized on TYPE.
BODY is executed with VAR and STREAM bound to the
value to be serialized and the output stream respectively."
  (with-gensyms (code)
    `(let ((,code (register-code ',type)))
       (declare (ignorable ,code))
       (defmethod internal-store-object ,@method-args ((,var ,type) ,buffer)
         ,@(unless method-args
             `((write-buf-byte ,code ,buffer)))
         ,@body))))

(defmacro defrestore ((type buff) &body body)
  (let ((fn-name (gensym (symbol-name type))))
    ;; The theory is that this will be easier to debug
    ;; than an anonymous function.
    `(flet ((,fn-name (,buff)
              ,@body))
       (let ((type-code (or (gethash ',type *registered-types*)
                            (error "Cannot define a restorer for this type."))))
         (when (gethash type-code *restore-funs*)
           (warn "Redefining restorer for type ~S ." ',type))
         (setf (gethash type-code *restore-funs*)
           #',fn-name)))))


(defun integer-or-symbolp (code)
  (member code `(,(gethash 'integer *registered-types*)
                 ,(gethash 'symbol *registered-types*)
                 ,(gethash 'character *registered-types*)
                 ,(gethash 'float *registered-types*))))

(defun restore-object (buff)
  "Reads a byte from buffer and calls the appropriate restorer
for the type returned or throws an error"
  (let* ((val (read-buf-byte buff))
         (restorer (gethash val *restore-funs*)))
    (if restorer
        (if (not (integer-or-symbolp val))
            (setf (gethash (incf *stored-counter*) *stored-values*)
                  (multiple-value-bind (x referrerp)
                      (funcall (the function restorer) buff)
                    (cond (referrerp
                           (setf *need-to-fix* t)
                           (ref-name x))
                          (t x))))
            (funcall (the function restorer) buff))
        (error 'restore-error
               :datum "No restore defined for type ~S."
               :args val))))

(let ((code (register-code 'referrer)))
  (defun store-referrer (obj buff)
    (write-buf-byte code buff)
    (store-32byte obj buff)))

(defrestore (referrer buff)
  (values (read-32-byte buff nil) t))

(defun store-object (obj buff)
  (multiple-value-bind (obj referrerp) (real-value obj)
    (if referrerp
        (store-referrer obj buff)
        (internal-store-object obj buff))))
  



;; ;; ;;
;; store non-return
(let ((code (register-code 'non-return)))
  (defun store-non-return (obj buff)
    (write-buf-byte code buff)
    (store-object obj buff)))

(defrestore (non-return buff)
  (restore-object buff)
  (restore-object buff))



(let ((code (register-code 'executable)))
  (defun store-executable (obj buff)
    (write-buf-byte code buff)
    (store-object obj buff)))

(defrestore (executable buff)
  (push (restore-object buff) *to-eval*)
  (restore-object buff))




;; integers
(defstore (obj integer buff)
  (loop for n = (abs obj) then (ash n -32)
    for counter from 0
    with collect = nil
    until (zerop n)
    do (push n collect)
    finally (progn
              (store-32byte (if (minusp obj)
                                (- counter)
                                counter)
                            buff)
              (dolist (num collect)
                (store-32byte num buff)))))

(defrestore (integer buff)
  (let ((count (read-32-byte buff))
        (result 0))
    (loop repeat (abs count) do
      (setf result (+ (ash result 32) (read-32-byte buff nil))))
    (if (minusp count)
        (- result)
        result)))

;; simple string
(defun output-simple-string (obj buff)
  (declare #-clisp (type simple-string obj)
           (optimize (speed 3) (safety 0) (space 0) (debug 0)))
  (let ((length (length obj)))
    (store-32byte length buff)
    (loop for x across obj do
      (write-buf-byte (char-code x) buff))))

#-clisp
(defstore (obj simple-string buff)
  (output-simple-string obj buff))

#+clisp (register-code 'simple-string)

(defun restore-simple-string (buff)
  (let* ((length (read-32-byte buff nil))
         (res (make-string length)))
    (loop for x from 1 to length do
      (setf (aref res (1- x)) (code-char (read-buf-byte buff))))
    res))

#-clisp
(defrestore (simple-string buff)
  (restore-simple-string buff))


;; floats
(defstore (obj float buff)
  (multiple-value-bind (significand exponent sign)
      (integer-decode-float obj)
    (write-buf-byte (float-type obj) buff)
    (store-object significand buff)
    (store-object exponent buff)
    (store-object sign buff)))

(defrestore (float buff)
  (let ((type (get-float-type (read-buf-byte buff))))
    (let ((significand (restore-object buff))
          (exponent (restore-object buff))
          (sign (restore-object buff)))
      (float (* (* significand (* 1.0d0 (expt 2 exponent))) sign) type))))


;; ratio
(defstore (obj ratio buff)
  (store-object (numerator obj) buff)
  (store-object (denominator obj) buff))

(defrestore (ratio buff)
  (/ (restore-object buff) (restore-object buff)))


;; chars
(defstore (obj character buff)
  (store-object (char-code obj) buff))

(defrestore (character buff)
  (code-char (restore-object buff)))
  



;; complex
(defstore (obj complex stream)
  (let ((real (realpart obj))
        (imag (imagpart obj)))
    (store-object real stream)
    (store-object imag stream)))
  
(defrestore (complex stream)
  (let ((real (restore-object stream))
        (imag (restore-object stream)))
    (complex real imag)))


;; symbols
(defstore (obj symbol buff)
  (let ((package (or (symbol-package obj)
                     *package*)))
    (output-simple-string (or (car (package-nicknames package))
                              (package-name package))
                          buff)
    (output-simple-string (symbol-name obj)
                          buff)))

(defrestore (symbol buff)
  (let ((package (restore-simple-string buff))
        (name (restore-simple-string buff)))
    (multiple-value-bind (a b)
        (intern name package)
      (declare (ignore b))
      a)))





;; lists
(defstore (obj cons buff)
  (store-object (car obj) buff)
  (store-object (cdr obj) buff))

(defrestore (cons stream)
  (cons (restore-object stream) (restore-object stream)))



;; pathnames
(defstore (obj pathname buff)
  (store-object (pathname-directory obj) buff)
  (store-object (pathname-name obj) buff)
  (store-object (pathname-type obj) buff)
  (store-object (pathname-version obj) buff))

(defrestore (pathname buff)
  (make-pathname :directory (restore-object buff)
                 :name (restore-object buff)
                 :type (restore-object buff)
                 :version (restore-object buff)))


;; hash tables
(defstore (obj hash-table buff)
  (store-object (hash-table-rehash-size obj) buff)
  (store-object (hash-table-rehash-threshold obj) buff)
  (store-object (hash-table-size obj) buff)
  (store-object (hash-table-test obj) buff)
  (store-object (hash-table-count obj) buff)
  (loop for key being the hash-keys of obj
    for value being the hash-values of obj do
    (store-object key buff)
    (store-object value buff)))

(defrestore (hash-table buff)
  (let ((rehash-size (restore-object buff))
        (rehash-threshold (restore-object buff))
        (size (restore-object buff))
        (test (restore-object buff))
        (count (restore-object buff)))
    (let ((hash (make-hash-table :test (symbol-function test)
                                 :rehash-size rehash-size
                                 :rehash-threshold rehash-threshold
                                 :size size)))
      (loop repeat count do
        (setf (gethash (restore-object buff) hash)
              (restore-object buff)))
      hash)))


;; objects
(defun store-type-object (obj buff)
  (let ((values (object-slot-and-vals obj)))
    (store-object (type-of obj) buff)
    (store-object (length values) buff)
    (dolist (val values)
      (store-object (car val) buff)
      (store-object (cadr val) buff))))

(defun restore-type-object (buff)
  (let* ((class (restore-object buff))
         (length (restore-object buff))
         (new-instance (make-instance class)))
    (loop repeat length do
      (setf (slot-value new-instance (restore-object buff))
            (restore-object buff)))
    new-instance))

(defstore (obj standard-object buff)
  (store-type-object obj buff))

(defstore (obj condition buff)
  (store-type-object obj buff))

(defstore (obj structure-object buff)
  (store-type-object obj buff))

(defrestore (structure-object buff)
  (restore-type-object buff))

(defrestore (condition buff)
  (restore-type-object buff))

(defrestore (standard-object buff)
  (restore-type-object buff))


;; classes
(defstore (obj standard-class buf)
  (when *store-class-superclasses*
    (loop for x in (class-direct-superclasses obj) do
      (when (and x (not (eq x (find-class 'standard-object))))
        (store-non-return x buf))))
  (store-object (get-class-details obj) buf))

(defrestore (standard-class buf)
  (let* ((vals (restore-object buf))
         (keywords '(#-lispworks :direct-default-initargs
                                 #+lispworks :default-initargs
                                 :direct-slots :direct-superclasses
                                 :metaclass))
         (final (apply #'append (mapcar #'list
                                        keywords
                                        (cdr vals)))))
    (if (find-class (car vals) nil)
        (if *nuke-existing-classes*
            (apply #'ensure-class (car vals) final)
            (find-class (car vals)))
        (apply #'ensure-class (car vals) final))))


;; built in classes
(defstore (obj built-in-class buff)
  (store-object (class-name obj) buff))

(defrestore (built-in-class buff)
  (find-class (restore-object buff)))

;; just in case it is not built in (cmucl, sbcl, lispworks)
(let ((code (register-code 'built-in-class)))
  (defmethod internal-store-object ((obj (eql (find-class 'hash-table))) buff)
    (write-buf-byte  code buff)
    (store-object 'cl:hash-table buff)))



;; arrays and vectors
(defstore (obj array buff)
  (let ((args (get-array-values obj)))
    ;; array-rank needs to be checked since in sbcl (pre 0.8.10) and cmucl since
    ;; multidimensional simple arrays loaded from fasl's have fill pointers
    (when (and (= (array-rank obj) 1)
               (array-has-fill-pointer-p obj))
      (setf args (list* :fill-pointer (fill-pointer obj) args)))
    (setf args (list* :element-type (array-element-type obj) args))
    (setf args (list* :adjustable (adjustable-array-p obj) args))
    (push (array-dimensions obj) args)
    (store-object args buff)))

(defrestore (array buff)
  (apply #'make-array (restore-object buff)))

#-clisp
(defstore (obj simple-vector buff)
  (let ((size (length obj)))
    (store-object size buff)
    (loop for x across obj do
      (store-object x buff))))

#+clisp (register-code 'simple-vector)

#-clisp
(defrestore (simple-vector buff)
  (let* ((size (restore-object buff))
         (res (make-array size)))
    (loop repeat size
      for i from 0 do
      (setf (aref res i) (restore-object buff)))
    res))

;; packages
(defstore (obj package buff)
  (store-object (package-name obj) buff))

(defrestore (package buff)
  (find-package (restore-object buff)))


;; EOF

