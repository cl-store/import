;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;; See the file LICENCE for licence information.

(in-package :cl-store)

(defvar *referrer-string* "%REFERRER-")
(defvar *stored-values* nil)
(declaim (type fixnum *stored-counter*))
(defvar *stored-counter* 0)


(defun referrerp (sym)
  (and (symbolp sym)
       (eq (symbol-package sym) #.(find-package :cl-store-referrers))
       (equal (subseq (symbol-name sym) 0 10)
              *referrer-string*)))

(defun referred-value (referrer hash)
  (gethash (read-from-string (subseq (symbol-name referrer) 10))
           hash))



(defgeneric fix-circularities (hash obj))

;; hash tables and objects require some extra fiddling.
(defmethod fix-circularities ((hash hash-table) (obj hash-table))
  (fix-circularities hash nil)
  (loop for key being the hash-keys of obj
    for val being the hash-values of obj do
    (fix-circularities hash val)
    (when (referrerp val)
      (setf (gethash key obj)
            (referred-value val hash)))))

(defmethod fix-circularities ((hash hash-table) (obj standard-class))
  nil)


(defmethod fix-circularities ((hash hash-table) (obj standard-object))
  (fix-circularities hash nil)
  (dolist (slot (mapcar #'slot-definition-name
                        (class-slots (class-of obj))))
    (when (slot-boundp obj slot)
      (fix-circularities hash (slot-value obj slot))
      (when (referrerp (slot-value obj slot))
        (setf (slot-value obj slot)
              (referred-value (slot-value obj slot) hash))))))

(defmethod fix-circularities ((hash hash-table) (obj structure-object))
  (fix-circularities hash nil)
  (dolist (slot (mapcar #'slot-definition-name
                        (class-slots (class-of obj))))
    (when (slot-boundp obj slot)
      (fix-circularities hash (slot-value obj slot))
      (when (referrerp (slot-value obj slot))
        (setf (slot-value obj slot)
              (referred-value (slot-value obj slot) hash))))))


(defmethod fix-circularities ((hash hash-table) obj)
  (loop for counter from 1 to (hash-table-count hash) do
    (let ((ref (gethash counter hash))
          changed)
      (when (referrerp ref)
        (setf (gethash counter hash)
              (referred-value ref hash)))
      (awhen (and (or (typep ref 'sequence)
                      (arrayp ref))
                  (pos-of ref))
        (cond
         ((eq it :last)
          (setf changed t)
          (setf (cdr (last ref))
                (referred-value (cdr (last ref)) hash)))
         ((and (listp ref) (numberp it))
          (setf changed t)
          (setf (nth it ref)
                (referred-value (nth it ref) hash)))
         ((and (arrayp ref) (numberp it))
          (setf changed t)
          (setf (row-major-aref ref it)
                (referred-value (row-major-aref ref it) hash)))
         (t nil)))
      (when changed
        ;; lets be sure.
        (fix-circularities hash obj)))))


(defun ref-name (x)
  (intern (format nil "%REFERRER-~D" x)
          :cl-store-referrers))


(defun pos-of (sequence)
  "Like position but it doens't choke on dotted lists"
  (when (and (listp sequence)
             (circular-listp sequence))
    (return-from pos-of nil))
  (labels ((inner (sequence counter)
             (cond ((atom sequence)
                    (when (referrerp sequence)
                      :last))
                   ((referrerp (car sequence))
                    counter)
                   (t (inner (cdr sequence) (1+ counter)))))
           (inner-array ()
             (loop for x from 0 upto (1- (array-total-size sequence)) do
               (if (referrerp (row-major-aref sequence x))
                   (return-from inner-array x)))))
      (cond ((and (listp sequence)
                  (atom (cdr (last sequence))))
             (inner sequence 0))
            ((vectorp sequence)
             (position-if #'referrerp sequence))
            ((arrayp sequence)
             (inner-array)))))








;; storing already seen objects


(defun seen (obj)
  (gethash obj *stored-values*))

(defun update-seen (obj)
  (declare (optimize (speed 3) (safety 0) (space 0) (debug 0)))
  (setf (gethash obj *stored-values*) (incf *stored-counter*))
  obj)

(defun needs-checkp (obj)
  (declare (optimize (speed 3) (safety 0) (space 0) (debug 0)))  
  (not (or (typep obj 'integer)
           (symbolp obj)
           (characterp obj)
           (floatp obj))))


;; instead of constructing symbols here we rather
;; just return a second value indicating we have
;; seen this object before and avoid interning unnecessary symbols
(defun real-value (obj)
  (declare (optimize (speed 3) (safety 0) (space 0) (debug 0)))
  (if (needs-checkp obj)
      (aif (seen obj)
           (values it t)
           (values (update-seen obj) nil))
      obj))

;; EOF
