;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;; See the file LICENCE for licence information.

(defpackage :cl-store-tests
  (:use :cl #+sbcl :sb-rt #-sbcl :rt :cl-store))

(in-package :cl-store-tests)


(rem-all-tests)
(defvar *test-file* "filetest.dat")

(defun restores (val)
  (store val *test-file*)
  (let ((restored (restore *test-file*)))
    (or (and (numberp val) (= val restored))
        (eq val restored)
        (eql val restored)
        (equal val restored)
        (equalp val restored))))

(defmacro deftestit (name val)
  `(deftest ,name (restores ,val) t))


;; integers
(deftestit integer.1 1)
(deftestit integer.2 0)
(deftestit integer.3 23423333333333333333333333423102334)
(deftestit integer.4 -2322993)
(deftestit integer.5 most-positive-fixnum)
(deftestit integer.6 most-negative-fixnum)
;; ratios
(deftestit ratio.1 1/2)
(deftestit ratio.2 234232/23434)
(deftestit ratio.3 -12/2)
(deftestit ratio.4 -6/11)
(deftestit ratio.5 23222/13)

;; complex numbers
(deftestit complex.1 #C(0 1))
(deftestit complex.2 #C(0.0 1.0))
(deftestit complex.3 #C(32 -23455))
(deftestit complex.4 #C(-222.32 2322.21))
(deftestit complex.5 #C(-111 -1123))
(deftestit complex.6 #C(-11.2 -34.5))


;; single-float
(deftestit single-float.1 3244.32)
(deftestit single-float.2 0.12)
(deftestit single-float.3 -233.001)
(deftestit single-float.4 most-positive-single-float)
(deftestit single-float.5 most-negative-single-float)

;; double-float
(deftestit double-float.1 2343.3d0)
(deftestit double-float.2 -1211111.3343d0)
(deftestit double-float.3 99999999999123456789012345678222222222222290.0987654321d0)
(deftestit double-float.4 -99999999999123456789012345678222222222222290.0987654321d0)
(deftestit double-float.5 most-positive-double-float)
(deftestit double-float.6 most-negative-double-float)

;; characters
(deftestit char.1 #\Space)
(deftestit char.2 #\f )
(deftestit char.3 #\Rubout)
(deftestit char.4 (code-char 255)) 


;; various strings
(deftestit string.1 "foobar")
(deftestit string.2 "how are you")
(deftestit string.3 "foo
bar")

(deftestit string.4
  (make-array 10 :initial-element #\f :element-type 'character
              :fill-pointer 3))

;; vectors
(deftestit vector.1 #(1 2 3 4))


(deftestit vector.2 (make-array 5 :element-type 'fixnum :initial-contents (list 1 2 3 4 5)))

(deftestit vector.3
  (make-array 5
              :element-type 'fixnum
              :fill-pointer 2
              :initial-contents (list 1 2 3 4 5)))


(deftestit vector.4 #*101101101110)
(deftestit vector.5 #*)
(deftestit vector.6 #())


;; arrays
(deftestit array.1
  (make-array '(2 2) :initial-contents '((1 2) (3 4))))

(deftestit array.2
  (make-array '(2 2) :initial-contents '((1 1) (1 1))))

(deftestit array.3
  (make-array '(2 2) :element-type '(mod 10) :initial-element 3))

(deftestit array.4
  (make-array  '(2 3 5) 
              :initial-contents
              '(((1 2 #\f 5 6) (#\Space "fpp" 4 1 0) ('d "foo" #() 3 -1))
                ((0 #\a #\b 4 #\q) (4 0 '(d) 4 1) (#\Newline 1 7 #\4 #\0)))))


;; symbols

(deftestit symbol.1  t)
(deftestit symbol.2  nil)
(deftestit symbol.3  :foo)
(deftestit symbol.4  'cl-store-tests::foo)
(deftestit symbol.5  'make-hash-table)


;; cons

(deftestit cons.1 '(1 2 3))
(deftestit cons.2 '((1 2 3)))
(deftestit cons.3 '(#\Space 1 1/2 1.3 #(1 2 3)))
  
(deftestit cons.4  '(1 . 2))
(deftestit cons.5  '(t . nil))



;; hash tables
(deftestit hash.1 (make-hash-table))

(deftestit hash.2
  (let ((val #.(let ((in (make-hash-table :test #'equal :rehash-threshold 0.4 :size 20
                                          :rehash-size 40)))
                 (dotimes (x 1000) (setf (gethash (format nil "~R" x) in) x))
                 in)))
    val))


;; packages
(deftestit package.1 (find-package :cl-store))


;; standard-object
(defun object-equalp (obj1 obj2)
  (typecase obj1
    ((or standard-object condition)
     (equalp (cl-store::object-slot-and-vals obj1)
             (cl-store::object-slot-and-vals obj2)))
    (t (equalp obj1 obj2))))

(defclass foo ()
  ((x :accessor get-x :initarg :x)))
(defclass bar (foo)
  ((y :accessor get-y :initform nil :initarg :y)))

(deftest standard-object.1
  (let ((val (store (make-instance 'foo :x 3) *test-file*)))
    (object-equalp val (restore *test-file*)))
  t)

(deftest standard-object.2
  (let ((val (store (make-instance 'bar
                                   :y (make-hash-table :test #'equal))
                    *test-file*)))
    (object-equalp val (restore *test-file*)))
  t)

#-clisp
(deftestit standard-class.1 (find-class 'foo))
#-clisp
(deftestit standard-class.2 (find-class 'bar))
  


;; conditions
(deftest condition.1
  (let ((val (handler-case (/ 1 0)
               (division-by-zero (c) (store c *test-file*)))))
    (object-equalp val (restore *test-file*)))
  t)

;; structure-object

(defstruct a
  a b c)

(defstruct (b (:include a))
  d e f)

#-(or clisp lispworks)
(deftestit structure-object.1 (make-a :a 1 :b 2 :c 3))
#-(or clisp lispworks)
(deftestit structure-object.2 (make-b :a 1 :b 2 :c 3 :d 4 :e 5 :f 6))
#-(or clisp lispworks)
(deftestit structure-object.3 (make-b :a 1 :b (make-a :a 1 :b 3 :c 2)
                                      :c #\Space :d #(1 2 3) :e (list 1 2 3)
                                      :f (make-hash-table)))

;; setf test
(deftestit setf.1 (setf (restore *test-file*) 0))
(deftestit setf.2 (incf (restore *test-file*)))
(deftestit setf.3 (decf (restore *test-file*) 2))

(deftestit pathname.1 #P"/home/foo")
(deftestit pathname.2 (make-pathname :name "foo"))


;; circular objects

(defvar circ1 (let ((x (list 1 2 3 4)))
                (setf (cdr (last x)) x)))
(deftest circ.1 (progn (store circ1 *test-file*)
                       (let ((x (restore *test-file*)))
                         (eq (cddddr x) x)))
  t)
                         
(defvar circ2 (let ((x (list 2 3 4 4 5)))
                        (setf (second x) x)))
(deftest circ.2 (progn (store circ2 *test-file*)
                       (let ((x (restore *test-file*)))
                         (eq (second x) x)))
  t)



(defvar circ3 (let ((x (list (list 1 2 3 4 )
                             (list 5 6 7 8)
                             9)))
                (setf (second x) (car x))
                (setf (cdr (last x)) x)
                x))

(deftest circ.3 (progn (store circ3 *test-file*)
                       (let ((x (restore *test-file*)))
                         (and (eq (second x) (car x))
                              (eq (cdddr x) x))))
  t)


(defvar circ4 (let ((x (make-hash-table)))
                (setf (gethash 'first x) (make-hash-table))
                (setf (gethash 'second x) (gethash 'first x))
                (setf (gethash 'inner (gethash 'first x)) x)
                x))

(deftest circ.4 (progn (store circ4 *test-file*)
                       (let ((x (restore *test-file*)))
                         (and (eq (gethash 'first x)
                                  (gethash 'second x))
                              (eq x
                                  (gethash 'inner
                                           (gethash 'first x))))))
  t)




(defvar circ5 (let ((x (make-instance 'bar)))
                (setf (get-y x) x)
                x))

(deftest circ.5 (progn (store circ5 *test-file*)
                       (let ((x (restore *test-file*)))
                         (eq x (get-y x))))
  t)


(defvar circ6 (let ((y (make-array '(2 2 2)
                                   :initial-contents '(((1 2) (3 4))
                                                       ((5 6) (7 8)))
                                   :element-type 'integer))) 
                (setf (aref y 1 1 1) y)
                y))


(deftest circ.6 (progn (store circ6 *test-file*)
                       (let ((x (restore *test-file*)))
                         (eq (aref x 1 1 1) x)))
  t)



(defvar circ7 (let ((x (make-a)))
                (setf (a-a x) x)))
#-(or clisp lispworks)
(deftest circ.7 (progn (store circ7 *test-file*)
                       (let ((x (restore *test-file*)))
                         (eq (a-a x) x)))
  t)



(defvar *count* 1)
(defvar *inc* 1)
(defclass foobar ()())
(defclass barfoo ()())

(defstore (obj foobar buff :before)
  (store-executable '(incf *count*) buff))

(deftest executable.1
  (progn (store (make-instance 'foobar) *test-file*)
         (restore *test-file*)
         (= *count* (incf *inc*)))
  t)


(defvar *hash* (make-hash-table))


(defstore (obj barfoo buff :before)
  (store-executable `(let ((foo *hash*))
                       (setf (gethash 1 foo)
                             ,obj)
                       (setf *hash* foo))
                    buff))

(deftest executable.2
  (progn (store (make-instance 'barfoo) *test-file*)
         (let ((x (restore *test-file*)))
           (eq x (gethash 1 *hash*))))
  t)


(defun run-tests ()
  #+sbcl(sb-rt:do-tests)
  #-sbcl(rt:do-tests)
  (when (probe-file *test-file*)
    (delete-file *test-file*)))

;; EOF


