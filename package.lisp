;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;; See the file LICENCE for licence information.


(defpackage :cl-store
  (:use :cl) 
  (:export :store
           :restore
           :defstore
           :defrestore
           :store-error
           :restore-error
           :internal-store-object
           :store-non-return
           :store-executable
           :store-object
           :restore-object
           :flush
           :fill-buffer
           :make-buffer
           :*full-write*
           :*store-class-slots*
           :*nuke-existing-classes*
           :*store-class-superclasses*)
  #+sbcl (:import-from :sb-mop
                       slot-definition-name
                       slot-value-using-class
                       slot-boundp-using-class
                       slot-definition-allocation
                       compute-slots
                       slot-definition-initform
                       slot-definition-initargs
                       slot-definition-name
                       slot-definition-readers
                       slot-definition-type
                       slot-definition-writers
                       class-direct-default-initargs
                       class-direct-slots
                       class-direct-superclasses
                       class-slots
                       ensure-class)
  
  #+cmu  (:import-from :pcl
                       slot-definition-name
                       slot-value-using-class
                       slot-boundp-using-class
                       slot-definition-allocation
                       compute-slots
                       slot-definition-initform
                       slot-definition-initargs
                       slot-definition-name
                       slot-definition-readers
                       slot-definition-type
                       slot-definition-writers
                       class-direct-default-initargs
                       class-direct-slots
                       class-direct-superclasses
                       class-slots
                       ensure-class)
  
  #+clisp (:import-from :clos
                        slot-value
                        std-compute-slots
                        slot-boundp
                        class-name
                        class-direct-default-initargs
                        class-direct-slots
                        class-slots
                        ensure-class)
    
  #+lispworks  (:import-from :clos
                             slot-definition-name
                             slot-value-using-class
                             slot-boundp-using-class
                             slot-definition-allocation
                             compute-slots
                             slot-definition-initform
                             slot-definition-initargs
                             slot-definition-name
                             slot-definition-readers
                             slot-definition-type
                             slot-definition-writers
                             class-direct-default-initargs
                             class-direct-slots
                             class-slots
                             class-direct-superclasses
                             ensure-class))



;; package used to unclutter cl-store by holding all %referrer symbols.
(defpackage :cl-store-referrers)

;; EOF
