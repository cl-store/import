;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;; See the file LICENCE for licence information.

(defpackage :cl-store.system
  (:use :cl :asdf))


(in-package :cl-store.system)

(defsystem cl-store
  :name "Store"
  :author "Sean Ross <sdr@jhb.ucs.co.za>"
  :maintainer "Sean Ross <sdr@jhb.ucs.co.za>"
  :version "0.1"
  :description "Serialization package"
  :long-description "Portable CL Package to serialize data types"
  :licence "MIT"
  :components ((:file "package")
               #+clisp(:file "fix-clisp" :depends-on "package")
               (:file "fast-io" :depends-on ("package"))
               (:file "utils" :depends-on ("fast-io"))
               (:file "circularities" :depends-on ("utils"))
               (:file "store" :depends-on ("circularities"))))


(defmethod perform :after ((o load-op) (c (eql (find-system :cl-store))))
  (provide 'cl-store))


(defmethod perform ((op test-op) (sys (eql (find-system :cl-store))))
  (oos 'load-op :cl-store-tests)
  (oos 'test-op :cl-store-tests))

(defsystem cl-store-tests
  #+sbcl :depends-on #+sbcl (sb-rt)
  :components ((:file "tests")))

(defmethod perform ((op test-op) (sys (eql (find-system :cl-store-tests))))
  (or (funcall (find-symbol "RUN-TESTS" "CL-STORE-TESTS"))
      (error "Test-op Failed.")))




;; EOF
